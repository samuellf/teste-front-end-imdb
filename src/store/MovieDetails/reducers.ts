import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../config/store';
import { MovieDetails } from '../../models/MovieDetails';
import { getMovieDetails } from './requests/getMovieDetails';

export interface MovieDetailsProps {
  movieDetails: MovieDetails;
  status: "initial" | "idle" | "loading" | "failed";
}

const initialState: MovieDetailsProps = {
  movieDetails: {} as MovieDetails,
  status: "initial"
}

const movieDetailsReducer = createSlice({
  name: 'movieDetails',
  initialState,
  reducers: {
    changeStatus: (state, action: PayloadAction<"initial" | "idle" | "loading" | "failed">) => {
      state.status = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getMovieDetails.pending, (state) => {
        state.status = "loading";
        state.movieDetails = {} as MovieDetails
      })
      .addCase(getMovieDetails.fulfilled, (state, action: PayloadAction<MovieDetails>) => {
        state.status = "idle";
        state.movieDetails = action.payload;
      });
  },
})

export default movieDetailsReducer.reducer

export const selectMovieDetails = (state: RootState) => state.movieDetails

export const { changeStatus } = movieDetailsReducer.actions