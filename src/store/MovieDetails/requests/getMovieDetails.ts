import { createAsyncThunk } from "@reduxjs/toolkit";

const API_KEY = process.env.VITE_API_KEY_IMDB

export const getMovieDetails = createAsyncThunk(
  'movies/getMovieDetails',
  async (id: string, { dispatch }) => {
    const url = `https://imdb-api.com/en/API/Title/${API_KEY}/${id}`

    const response = await fetch(url, { method: 'GET' })

    const data = await response.json();

    if(data.errorMessage){
      return {}
    }

    return data;
  }
)