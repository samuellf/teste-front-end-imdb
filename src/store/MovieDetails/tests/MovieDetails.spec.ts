import { MovieDetails } from '../../../models/MovieDetails';
import moviesListReducer, { MovieDetailsProps, changeStatus } from '../reducers';

describe("MovieDetail reducer", () => {
  const state: MovieDetailsProps = {
    movieDetails: {} as MovieDetails,
    status: "initial"
  };
  
  it("should handle initial state", () => {
    const initialState: MovieDetailsProps = state;
    const action = { type: "unknown" };
    const expectedState = initialState;
    expect(moviesListReducer(initialState, action)).toEqual(expectedState);
  });

  describe("Change status", () => {
    it("should handle change status to loading", () => {
      const initialState: MovieDetailsProps = {...state, status: "initial"};
      const action = changeStatus("loading")
      const expectedState: MovieDetailsProps = {...state, status: "loading"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });
    
    it("should handle change status to failed", () => {
      const initialState: MovieDetailsProps = {...state, status: "loading"};
      const action = changeStatus("failed")
      const expectedState: MovieDetailsProps = {...state, status: "failed"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });

    it("should handle change status to idle", () => {
      const initialState: MovieDetailsProps = {...state, status: "loading"};
      const action = changeStatus("idle")
      const expectedState: MovieDetailsProps = {...state, status: "idle"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });

    it("should handle change status to initial", () => {
      const initialState: MovieDetailsProps = {...state, status: "idle"};
      const action = changeStatus("initial")
      const expectedState: MovieDetailsProps = {...state, status: "initial"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });
  })
});

export {}