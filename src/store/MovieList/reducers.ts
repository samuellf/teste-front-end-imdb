import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../config/store';
import { MovieList } from '../../models/MovieList';
import { getMoviesList } from './requests/getMoviesList';

export interface MovieListProps {
  query: string;
  moviesList: MovieList[];
  status: "initial" | "idle" | "loading" | "failed";
}

const initialState: MovieListProps = {
  query: "",
  moviesList: [],
  status: "initial"
}

const movieListReducer = createSlice({
  name: 'movieList',
  initialState,
  reducers: {
    setSearchQuery: (state, action: PayloadAction<string>) => {
      state.query = action.payload;
    },
    changeStatus: (state, action: PayloadAction<"initial" | "idle" | "loading" | "failed">) => {
      state.status = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getMoviesList.pending, (state) => {
        state.status = "loading";
        state.moviesList = []
      })
      .addCase(getMoviesList.fulfilled, (state, action: PayloadAction<MovieList[]>) => {
        state.status = "idle";
        state.moviesList = action.payload;
      });
  },
})

export default movieListReducer.reducer

export const selectMoviesList = (state: RootState) => state.moviesList

export const { changeStatus, setSearchQuery } = movieListReducer.actions