import moviesListReducer, { setSearchQuery, MovieListProps, changeStatus } from '../reducers';

describe("MovieList reducer", () => {
  const state: MovieListProps = {
    query: "",
    moviesList: [],
    status: "initial"
  };
  
  it("should handle initial state", () => {
    const initialState: MovieListProps = state;
    const action = { type: "unknown" };
    const expectedState = initialState;
    expect(moviesListReducer(initialState, action)).toEqual(expectedState);
  });

  it("should handle setSearchQuery", () => {
    const initialState: MovieListProps = {...state, query: ""};
    const action = setSearchQuery("Marvel")
    const expectedState: MovieListProps = {...state, query: "Marvel"};
    expect(moviesListReducer(initialState, action)).toEqual(expectedState);
  });

  describe("Change status", () => {
    it("should handle change status to loading", () => {
      const initialState: MovieListProps = {...state, status: "initial"};
      const action = changeStatus("loading")
      const expectedState: MovieListProps = {...state, status: "loading"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });
    
    it("should handle change status to failed", () => {
      const initialState: MovieListProps = {...state, status: "loading"};
      const action = changeStatus("failed")
      const expectedState: MovieListProps = {...state, status: "failed"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });

    it("should handle change status to idle", () => {
      const initialState: MovieListProps = {...state, status: "loading"};
      const action = changeStatus("idle")
      const expectedState: MovieListProps = {...state, status: "idle"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });

    it("should handle change status to initial", () => {
      const initialState: MovieListProps = {...state, status: "idle"};
      const action = changeStatus("initial")
      const expectedState: MovieListProps = {...state, status: "initial"};
      expect(moviesListReducer(initialState, action)).toEqual(expectedState);
    });
  })
});

export {}