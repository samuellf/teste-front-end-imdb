import { createAsyncThunk } from "@reduxjs/toolkit";

const API_KEY = process.env.VITE_API_KEY_IMDB

export const getMoviesList = createAsyncThunk(
  'movies/getMoviesList',
  async (movieTitle: string, { dispatch }) => {
    const url = `https://imdb-api.com/en/API/SearchMovie/${API_KEY}/${movieTitle}`

    const response = await fetch(url, { method: 'GET' })

    const data = await response.json();

    return data.results;
  }
)