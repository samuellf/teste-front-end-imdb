export const movieListMock = [
  {
    "id": "tt5356486",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BMDNhNmRmMDEtMTU4NC00YzQxLTg3MWUtZTMzMmJkMGUwMjkwXkEyXkFqcGdeQXVyNjAyNDkzMDc@._V1_Ratio1.7727_AL_.jpg",
    "title": "Marvel",
    "description": "(2015) (Short)"
  },
  {
    "id": "tt20678200",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/nopicture.jpg",
    "title": "Marvel",
    "description": "(2021) (Podcast Episode) - Make It Reign (2020) (Podcast Series)"
  },
  {
    "id": "tt3480822",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BNjRmNDI5MjMtMmFhZi00YzcwLWI4ZGItMGI2MjI0N2Q3YmIwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_Ratio0.7273_AL_.jpg",
    "title": "Black Widow",
    "description": "(2021) aka \"Marvel Studios' Black Widow\""
  },
  {
    "id": "tt1211837",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BNjgwNzAzNjk1Nl5BMl5BanBnXkFtZTgwMzQ2NjI1OTE@._V1_Ratio0.7273_AL_.jpg",
    "title": "Doctor Strange",
    "description": "(2016) aka \"Marvel's Doctor Strange\""
  },
  {
    "id": "tt4154664",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BMTE0YWFmOTMtYTU2ZS00ZTIxLWE3OTEtYTNiYzBkZjViZThiXkEyXkFqcGdeQXVyODMzMzQ4OTI@._V1_Ratio0.7273_AL_.jpg",
    "title": "Captain Marvel",
    "description": "(2019)"
  },
  {
    "id": "tt10857164",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BZmQ3OTZkNDUtNTU0Mi00ZjE4LTgyNTUtY2E4NWRmNDUxMzkyXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_Ratio0.7273_AL_.jpg",
    "title": "Ms. Marvel",
    "description": "(2022) (TV Mini Series)"
  },
  {
    "id": "tt10676048",
    "resultType": "Title",
    "image": "https://imdb-api.com/images/original/MV5BNjQ4NzQ2MmUtMTUwYi00ZTVhLWI5Y2MtYWM3ZmM5MTc1OGEyXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_Ratio0.7273_AL_.jpg",
    "title": "The Marvels",
    "description": "(2023)"
  }
]