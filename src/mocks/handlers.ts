import { rest } from "msw";
import { movieListMock } from './MovieList';

const API_KEY = process.env.VITE_API_KEY_IMDB

const URL = `https://imdb-api.com/en/API/SearchMovie/${API_KEY}/Marvel`;

export const handlers = [
  rest.get(`${URL}`, (_, res, ctx) => {
    return res(
      ctx.json(movieListMock),
      ctx.delay(150)
    );
  }),
];