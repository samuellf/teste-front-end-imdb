import { ChangeEvent } from 'react';
import styles from './styles.module.css'

type SearchProps = {
  onChangeSearch: (e: ChangeEvent<HTMLInputElement>) => void;
  handleSearch: () => void
};


export function SearchBar ({ handleSearch, onChangeSearch }: SearchProps) {
  return (
    <div className={styles.container}>
      <input 
        data-testid="search"
        type="text" 
        placeholder='Digite o nome do filme...'
        className={styles.input}
        onChange={onChangeSearch}
      />
      <button className={styles.button} onClick={handleSearch}>
        Buscar
      </button>
    </div>
  )
}