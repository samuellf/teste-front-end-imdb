import { useNavigate } from 'react-router-dom';
import { MovieList } from '../../../models/MovieList';
import styles from './styles.module.css'


export function MovieSimpleCard ({ id, image, title, description }: MovieList) {
  let navigate = useNavigate();

  const sendToMovieDetail = () => {
    navigate(`/movie/${id}`)
  }

  return (
    <div className={styles.card} onClick={sendToMovieDetail}>
      <img alt={title} src={image} className={styles.img}/>
      <p className={styles.movieName}>
        {title}
      </p>
      <span className={styles.movieDescription}>
        {description}
      </span>
    </div>
  )
}