import { ActorList } from '../../../models/MovieDetails'
import { ShareButton } from '../../ShareButton'
import styles from './styles.module.css'

interface DetailsCardProps {
  image: string
  title: string
  year: string
  plot: string
  actorList: ActorList[]
}

export function MovieDetailsCard ({ image, title, year, plot, actorList }: DetailsCardProps) {
  
  return (
    <div className={styles.card}>
      <div>
        <img alt={"title"} src={image} className={styles.imgMovie}/>
        <div className={styles.containerShareLink}>
          <ShareButton />
        </div>      
      </div>

      <div className={styles.containerDetails}>
        <p className={styles.movieName}>
          {title}
        </p>
        <p className={styles.movieYear}>
          Ano: {year}
        </p>
        <p className={styles.movieDescription}>
          Descrição: {plot}
        </p>

          <p className={styles.titleActor}>Atores:</p>

          <div className={styles.containerActor}>
            {
              actorList.map((actor) => (
                <div key={actor.id}>
                  <img alt={"title"} src={actor.image} className={styles.imageActor}/>
                  <p className={styles.nameActor}>
                    {actor.name}
                  </p>
                  {
                    actor.asCharacter !== "" ? (
                      <p className={styles.characterActor}>
                        ({actor.asCharacter})
                      </p>
                    ) : null
                  }
                </div>
              ))
            }
          </div>
      </div>

    </div>
  )
}