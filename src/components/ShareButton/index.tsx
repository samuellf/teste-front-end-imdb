import styles from './styles.module.css'

export function ShareButton () {

  const shareLink = async () => {
    const shareData = {
      title: 'IMDB - Search Movies',
      text: "Busque e veja os detalhes dos melhores filmes aqui",
      url: window.location.href,
    }

    await navigator.share(shareData)
  }

  return (
    <button className={styles.button} onClick={shareLink}>
      Compartilhar
    </button>
  )
}