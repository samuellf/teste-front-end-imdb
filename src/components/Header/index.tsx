import { Link } from 'react-router-dom'
import { Logo } from '../../components/Logo'
import styles from './styles.module.css'

export function Header () {
  return (
    <div className={styles.containerLogo}>
      <Link to="/">
        <Logo size={150}/>
      </Link>
    </div>
  )
}