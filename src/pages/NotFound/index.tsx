import { Link } from 'react-router-dom';
import styles from './styles.module.css'

export function NotFound() {
  return (
    <div className={styles.container}>
      <h1 className={styles.notFoundPage}>404 - Página não encontrada!</h1>

      <Link to="/">
        <span className={styles.linkToHome}>Voltar para busca</span>
      </Link>
    </div>
  );
}