import { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Loader from '../../components/Loader';
import { MovieDetailsCard } from '../../components/Movie/MovieDetailsCard';
import { useAppDispatch, useAppSelector } from '../../config/hooks';
import { selectMovieDetails } from '../../store/MovieDetails/reducers';
import { getMovieDetails } from '../../store/MovieDetails/requests/getMovieDetails';
import { isEmpty } from '../../utils/isEmptyObject';

import styles from './styles.module.css'

export function MovieDetails () {
  let navigate = useNavigate();
  let { id } = useParams();
  const dispatch = useAppDispatch();
  const { status, movieDetails } = useAppSelector(selectMovieDetails)


  useEffect(() => {
    if(id){
      dispatch(getMovieDetails(id))
    }
  }, [])

  function goToHome () {
    navigate("/", { replace: true });
  }

  return (
    <section>
      {
        status === "loading" ? (
          <div className={styles.containerLoader}>
            <Loader />
          </div>
        ) : null
      }

      {
        !isEmpty(movieDetails) ? (
          <MovieDetailsCard
            image={movieDetails.image}
            title={movieDetails.title}
            year={movieDetails.year}
            plot={movieDetails.plot}
            actorList={movieDetails.actorList}
          />
        ) : null
      }

      {
        status !== "initial" && status !== "loading" &&  isEmpty(movieDetails) ? (
          <>          
            <p className={styles.notFoundItem}>
              O filme não foi encontrado! Por favor, tente novamente.
            </p>
            <div className={styles.containerLink} onClick={goToHome}>
              <span className={styles.linkToHome}>Voltar para busca</span>
            </div>
          </>
        ) : null
      }
    </section>
  )
}