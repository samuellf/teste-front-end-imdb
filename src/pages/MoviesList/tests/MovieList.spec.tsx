import { render, screen, fireEvent } from "../../../config/test.utils";
import { sleep } from '../../../utils/sleep';

import { MoviesList }from "../index";

const { click, change } = fireEvent;
const { getByText, getByTestId, getByPlaceholderText, getAllByText } = screen;

describe("MoviesList Component", () => {
  it("should show button search", async () => {
    render(<MoviesList />);
    expect(getByPlaceholderText(/Digite o nome do filme.../i)).toBeInTheDocument();
    expect(getByText(/Buscar/i)).toBeInTheDocument();
  });

  it("should show movies list", async () => {
    render(<MoviesList />);
    change(getByTestId("search"), { target: { value: "Marvel" } });
    click(getByText(/Buscar/i))
    await sleep(200)
    const card = getAllByText("Ms. Marvel")
    expect(card).toHaveLength(1);
  });
})