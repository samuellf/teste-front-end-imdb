import { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import Loader from '../../components/Loader';
import { MovieSimpleCard } from '../../components/Movie/MovieSimpleCard';
import { SearchBar } from '../../components/SearchBar';
import { ShareButton } from '../../components/ShareButton';
import { useAppDispatch, useAppSelector } from '../../config/hooks';
import { selectMoviesList, setSearchQuery } from '../../store/MovieList/reducers';
import { getMoviesList } from '../../store/MovieList/requests/getMoviesList';
import styles from './styles.module.css'

export function MoviesList() {
  let [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const { query, moviesList, status } = useAppSelector(selectMoviesList)

  useEffect(() => {
    const paramsSearch = searchParams.get("search")

    if(paramsSearch){
      handleSearchPerParams(paramsSearch)
    }
  }, [])

  const onChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearchQuery(e.target.value));
  };

  const handleSearch = () => {
    dispatch(getMoviesList(query));
    setSearchParams({ search: query })
  }

  const handleSearchPerParams = (titleMovie: string) => {
    dispatch(getMoviesList(titleMovie));
  }

  return (
    <div className={styles.container}>
      <SearchBar handleSearch={handleSearch} onChangeSearch={onChangeSearch} />
      {
        status === "loading" ? (
          <div>
            <Loader />
            <p className={styles.textLoading}>Carregando...</p>
          </div>
        ) : null
      }

      {
        moviesList.length > 0 ? (
          <>          
            <section className={styles.containerList}>
              {
                moviesList.map((movie) => (
                  <MovieSimpleCard
                    key={movie.id}
                    description={movie.description}
                    id={movie.id}
                    image={movie.image}
                    title={movie.title}
                  />
                ))
              }
            </section>
            <div className={styles.containerShareLink}>
              <ShareButton />
            </div>
          </>
        ) : null
      }

      {
        moviesList.length === 0 && status !== "loading" && status !== "initial" ? (
          <p className={styles.notFoundItem}>
            Nenhum filme foi encontrado! Tente novamente digitando outro nome.
          </p>
        ) : null
      }
    </div>
  );
}

