import { Header } from './components/Header'
import { RoutesConfig } from './routes'
import './styles/global.css'

function App() {

  return (
    <main className='containerApp'>
      <Header />
      <RoutesConfig />
    </main>
  )
}

export default App
