import { ReactElement, ReactNode } from 'react';
import { render as rtlRender } from "@testing-library/react";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

import moviesListReducer from '../store/MovieList/reducers'
import { BrowserRouter } from 'react-router-dom';

function render(
  ui: ReactElement,
  {
    preloadedState,
    store = configureStore({ 
        reducer: { 
          moviesList: moviesListReducer 
        }, 
        preloadedState
      }
    ),
    ...renderOptions
  }: any = {}
) {
  function Wrapper({ children }: { children: ReactNode }) {
    return (
      <Provider store={store}>      
        <BrowserRouter>
          {children}
        </BrowserRouter>
      </Provider>
    );
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}

export * from "@testing-library/react";

export { render };
