import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';

import moviesListReducer from '../store/MovieList/reducers'
import movieDetailsReducer from '../store/MovieDetails/reducers'

export const store = configureStore({
  reducer: {
    moviesList: moviesListReducer,
    movieDetails: movieDetailsReducer,
  },
  devTools: true,
});

export type AppDispatch = typeof store.dispatch;

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

