import { Route, Routes } from "react-router-dom";
import { MovieDetails } from '../pages/MovieDetails';
import { MoviesList } from '../pages/MoviesList';
import { NotFound } from '../pages/NotFound';


export const RoutesConfig = () => {
  return (
    <Routes>
      <Route path="/" element={<MoviesList />} />
      <Route path="/movie/:id" element={<MovieDetails />} />
      <Route path="/*" element={<NotFound />} />
    </Routes>
  );
}