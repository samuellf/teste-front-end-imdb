# Teste front end

## :rocket: Sobre a aplicação

Todas as funcionalidades foram implementadas, como:
 - Buscar pelo nome do filme
 - Listagem de filmes
 - Exibição dos detalhes do filme escolhido
 - Compartilhamento da url com os dados buscando na API

## :rocket: F.A.Q

Vimos que tem 1 teste falhando, o que houve?
: Tentei fazer alguns testes de integração, porém, por algum motivo o msw não estava interceptando as requisições, então, como não encontrei a solução a tempo, deixei o código pra que pudessem entender a minha linha de raciocínio!

Teve alguma dificuldade?
: No geral, tudo foi muito tranquilo e sem nenhum problema. Porém, a parte de configuração dos teste e implementação dos testes de integração deu um pouco de trabalho sim e foram eles que consumiram a maior parte do desenvolvimento.

Você tentou fazer o build da aplicação?
: Sim! Inclusive, você consegue encontrá-la na url a seguir: [IMDB - Search Movies](https://effervescent-hamster-a228e5.netlify.app/)

## :fire: Como Usar

### Pré-requisitos

- [Node.js](https://nodejs.org/);
- [NPM](https://www.npmjs.com/get-npm) ou [Yarn](https://classic.yarnpkg.com/pt-BR/docs/install/);


1.  Faça um clone:

```shell
$ git clone https://gitlab.com/samuellf/teste-front-end-imdb.git
OU
$ git clone git@gitlab.com:samuellf/teste-front-end-imdb.git
```

3. Executando o projeto:

```
# Instale as dependências
$ npm install

# Altere o .env
Na aplicação existe um arquivo .env.example, coloque a sua API KEY na frente do ( VITE_API_KEY_IMDB = ) e renomei o arquivo para apenas .env!
OU
crie um arquivo do zero seguindo as mesmas instruções.

# Inicie a aplicação web
$ npm run dev
```

4. Build da aplicação:

```
# Gerar o build da aplicação
$ npm run build
```